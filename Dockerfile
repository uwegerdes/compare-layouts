#
# Dockerfile for compare-layouts-publish
#

FROM node:4
# contains: bzip2 curl git imagemagick make openssh-client python subversion wget

MAINTAINER Uwe Gerdes <entwicklung@uwegerdes.de>

ENV DEBIAN_FRONTEND noninteractive
ENV APP_DIR /usr/src/app

# apt-cacher-ng docker on 172.17.0.x
RUN echo 'Acquire::http { Proxy "http://172.17.0.3:3142"; };' >> /etc/apt/apt.conf.d/01proxy

RUN apt-get update && \
	apt-get dist-upgrade -y && \
	apt-get install -y iceweasel xvfb sudo && \
	rm -rf /var/lib/apt/lists/*

RUN npm --loglevel warn install -g phantomjs@1.9.19
RUN npm --loglevel warn --proxy http://192.168.1.18:3143 --https-proxy http://192.168.1.18:3143 --strict-ssl false install -g casperjs slimerjs

RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}

COPY package.json ${APP_DIR}
RUN npm --loglevel warn --proxy http://192.168.1.18:3143 --https-proxy http://192.168.1.18:3143 --strict-ssl false install
COPY . ${APP_DIR}

CMD [ "npm", "start" ]
# end FROM node:4-publish

RUN useradd -m -s /bin/bash node

RUN chown -R node ${APP_DIR}

ENV HOME ${APP_DIR}
USER node

VOLUME ["${APP_DIR}/config", "${APP_DIR}/results"]

EXPOSE 3000 35730


